using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Taco.Timeline;
using UnityEngine.Playables;

public class TimelinePlayerSample : TimelinePlayer
{
    public Timeline[] Timelines;

    protected override void Update()
    {
        base.Update();

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Timeline timeline = Instantiate(Timelines[0]);
            AddTimeline(timeline);
            timeline.OnDone += () => RemoveTimeline(timeline);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Timeline timeline = Instantiate(Timelines[1]);
            AddTimeline(timeline);
            timeline.OnDone += () => RemoveTimeline(timeline);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Timeline timeline = Instantiate(Timelines[2]);
            AddTimeline(timeline);
            timeline.OnDone += () => RemoveTimeline(timeline);
        }
    }
    public override void AddTimeline(Timeline timeline)
    {
        base.AddTimeline(timeline);
        CtrlPlayable.Pause();

    }
    public override void RemoveTimeline(Timeline timeline)
    {
        base.RemoveTimeline(timeline);
        CtrlPlayable.Play();
    }
}
