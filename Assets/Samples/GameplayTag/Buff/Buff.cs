using System;
using System.Collections.Generic;
using UnityEngine;
using Taco.Gameplay;

public class Buff : ScriptableObject
{
    public string Name;
    public GameplayTagContainer Tag = new GameplayTagContainer();
    public GameplayTagContainer BlockToAddTag = new GameplayTagContainer();
    public GameplayTagContainer AddedToRemoveTag = new GameplayTagContainer();

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Assets/Create/Taco/Buff")]
    public static void CreateBuff()
    {
        Buff buff = CreateInstance<Buff>();
        string path = UnityEditor.AssetDatabase.GetAssetPath(UnityEditor.Selection.activeObject);
        string assetPathAndName = UnityEditor.AssetDatabase.GenerateUniqueAssetPath(path + "/New Buff.asset");
        UnityEditor.AssetDatabase.CreateAsset(buff, assetPathAndName);
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.AssetDatabase.Refresh();
        UnityEditor.Selection.activeObject = buff;
    }
#endif
}