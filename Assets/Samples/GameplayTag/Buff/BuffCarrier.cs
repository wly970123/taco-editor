using System.Collections.Generic;
using Taco.Gameplay;
using UnityEngine;

public class BuffCarrier : MonoBehaviour
{
    [ScriptableObjectField(typeof(Buff))]
    public List<Buff> BuffList;

    [ObjectField("OnDropBuff")]
    public Buff BuffToAdd;

    public void OnDropBuff(object a)
    {
        if(a != null)
        {
            BuffToAdd = null;
            AddBuff(a as Buff);
        }
    }

    public void AddBuff(Buff buffToAdd)
    {
        if (BuffList.Contains(buffToAdd))
            return;

        //to block?
        foreach (var buff in BuffList)
        {
            if (buffToAdd.Tag.PartChildOf(buff.BlockToAddTag))
            {
                Debug.Log($"<{buff}>blocked<{buffToAdd}>to add");
                return;
            }
        }

        //add 
        BuffList.Add(buffToAdd);

        //to remove
        for (var i = BuffList.Count - 1; i >= 0; i--)
        {
            if (BuffList[i].Tag.PartChildOf(buffToAdd.AddedToRemoveTag))
            {
                Debug.Log($"<{BuffList[i]}>was removed because<{buffToAdd}>was added");
                BuffList.RemoveAt(i);
            }
        }
    }
}
